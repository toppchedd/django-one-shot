from django.urls import path
from todos.views import (
    todo_list,
    show_todo,
    todo_list_create,
    todo_list_update,
    delete_list,
    create_item,
    todo_item_update,
)


urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", show_todo, name="todo_list_detail"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>/edit/", todo_list_update, name="todo_list_update"),
    path("<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("items/create/", create_item, name="todo_item_create"),
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
]
