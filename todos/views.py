from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list(request):
    todolist = TodoList.objects.all()
    context = {
        "todo_list": todolist,
    }
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todoslist = get_object_or_404(TodoList, id=id)
    context = {
        "todo_obj": todoslist,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    update = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=update)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.id)
    else:
        form = TodoListForm(instance=update)
    context = {
        "todo_list": update,
        "form": form,
    }
    return render(request, "todos/update.html", context)


def delete_list(request, id):
    delete = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form
        }
    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    update = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=update)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.id)
    else:
        form = TodoItemForm(instance=update)
        context = {
            "todoitem": update,
            "form": form
            }
    return render(request, "todos/items/edit.html", context)


# html template for todo item update
# list view link


# pseudocode input / output
# desired product
# breakdown how to get there from input
# what code/syntax is used to do these things
# user url > project urls > app urls >
# views aggregates data from >
# forms > models >
# Views RENDERS HTML


# method model display name
